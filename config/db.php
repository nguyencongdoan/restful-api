<?php
    // ket noi database theo PDO

    class db
    {
        private $conn;
        private $servername = "localhost";
        private $username = "username";
        private $password = "password";
        private $dbname = "restful_php_api";

        public function connect()
        {
            $this->conn = null;

            try {
                $this->conn = new PDO("mysql:host=" . $this->servername . "; dbname=" . $this->dbname . "", $this->username, $this->password);
                // set the PDO error mode to exception
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                // echo "Kết nối thành công";
            } catch (PDOException $e) {
                echo "Kết nối thất bại: " . $e->getMessage();
            }
            return $this->conn;
        }
    }
?>
