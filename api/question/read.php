<?php
    header('Access-Control-Allow-Origin:*');
    header('Content-Type: application/json');

    include('../../config/db.php');
    include_once('../../model/question.php');

    $db = new db(); // goi class db o db.php
    $connect =  $db->connect();

    $question = new Question($connect); //goi class question o model/question.php
    $read = $question->read();

    $num = $read->rowCount();

    if ($num > 0) {
        $question_arr = [];
        $question_arr['data'] = [];

        while ($row = $read->fetch(PDO::FETCH_ASSOC)) {
            extract($row);

            $question_item = array(
                'id_cauhoi' => $id_cauhoi,
                'title' => $title,
                'cau_a' => $cau_a,
                'cau_b' => $cau_b,
                'cau_c' => $cau_c,
                'cau_d' => $cau_d,
                'cau_dung' => $cau_dung
            );
            array_push($question_arr['data'], $question_item);
        }
        echo json_encode($question_arr);
    }
