const cautraloi = document.querySelectorAll('.cautraloi');
const submit = document.getElementById('submit');

let cau_hientai = 0;
let dem = 0;

load_cauhoi();

function load_cauhoi() {
    remove_answers();
    fetch('http://localhost/restful_api/api/question/read.php')
    .then(res => res.json())
    .then(data => {
        console.log(data)
        const cauhoi = document.getElementById('title');
        
        const a_cautraloi = document.getElementById('a_cautraloi');
        const b_cautraloi = document.getElementById('b_cautraloi');
        const c_cautraloi = document.getElementById('c_cautraloi');
        const d_cautraloi = document.getElementById('d_cautraloi');

        // chuoi json data: { "cau1: a, ..."}
        const get_cauhoi = data.data[cau_hientai]
        console.log(get_cauhoi)

        cauhoi.innerText = get_cauhoi.title;
        a_cautraloi.innerText = get_cauhoi.cau_a;
        b_cautraloi.innerText = get_cauhoi.cau_b;

        if(get_cauhoi.cau_c != null) {
            c_cautraloi.innerText = get_cauhoi.cau_c;
        }
        else{
            document.getElementById('cau_c').style.display = 'none';
        }

        if(get_cauhoi.cau_d != null) {
            d_cautraloi.innerText = get_cauhoi.cau_d;
        }
        else{
            document.getElementById('cau_d').style.display = 'none';
        }

    })
    .catch(error => console.log(error));
}

// remove cau tra loi
function remove_answers(){
    cautraloi.forEach((ans) =>{
        ans.checked = false;  
    });
}

submit.addEventListener('click', () => {
    cau_hientai++;
    // if(cau_hientai < data_cauhoi.lenght) {
        load_cauhoi();
    // }
});